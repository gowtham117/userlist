package models

import play.api.data.Form
import play.api.data.Forms._


case class User(age: String, name: String) {}
case class Users(userList: List[User]) {}

object Users{
  val usersForm: Form[Users] = Form(
    mapping(
      "users" -> list(mapping(
        "age" -> text,
        "name" -> text
      )(User.apply)(User.unapply))
    )(Users.apply)(Users.unapply)
  )
}