package controllers

import javax.inject.Inject

import models.{User, Users}
import play.api.i18n.I18nSupport
import play.api.mvc._

class UserController @Inject()(cc: ControllerComponents) extends AbstractController(cc) with I18nSupport {

  private var userList = List(User("1","a"), User("2","b"), User("3","c"))

  def userIndex = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.userIndex(userList, Users.usersForm))
  }

  def save = Action { request =>
    println(s"Enter the method")
    val users = Users.usersForm.bindFromRequest()(request).get
    userList = users.userList
    println(s"users : ${users}")
    Redirect(routes.UserController.userIndex())
  }

}
